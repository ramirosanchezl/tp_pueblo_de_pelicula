﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using pueblodepelicula.DAL;
using pueblodepelicula.Models;

namespace pueblodepelicula.Controllers
{
    public class HomeController : Controller
    {

        private DBContext db = new DBContext();


        public ActionResult Index()
        {
            var movies = db.Movies;
            var theatres = db.Theatres.ToList();
            theatres.OrderBy(t => t.Name);
            ViewBag.Theatres = db.Theatres.ToList();
            var orderedMovies = movies.OrderBy(m => m.Name);
            return View(orderedMovies.ToList());

        }

        [HttpPost]
        public ActionResult IndexSearch(int? theatreID, string Usertext)
        {
            var userTextLower = Usertext.ToLower();
            var theatres = db.Theatres.ToList();
            theatres.OrderBy(t => t.Name);
            ViewBag.Theatres = theatres;

            var moviesInSearch = new HashSet<Movie>();


            if (db.Theatres.Find(theatreID) != null && userTextLower != null)
            {
                var movies = db.Cinemas
                .Include(c => c.Screenings.Select(m => m.Movie))
                .Where(t => t.TheatreID == theatreID);
                foreach (var moviesaux in movies)
                {
                    foreach (var screeningsAux in moviesaux.Screenings)
                    {
                        if (screeningsAux.Movie.Name.ToLower().Contains(userTextLower))
                        {
                            moviesInSearch.Add(screeningsAux.Movie);
                        }


                        if (screeningsAux.Movie is Commercial)
                        {
                            var movie = (Commercial)screeningsAux.Movie;
                            if (movie.Actor.ToLower().Contains(userTextLower))
                            {
                                moviesInSearch.Add(screeningsAux.Movie);
                            }

                        }

                        if (screeningsAux.Movie is Documentary)
                        {
                            var movie = (Documentary)screeningsAux.Movie;
                            if (movie.Category.Equals(userTextLower))
                            {
                                moviesInSearch.Add(screeningsAux.Movie);
                            }
                        }

                    }
                }

                moviesInSearch.OrderBy(m => m.Name);
                return View("Index", moviesInSearch);
            }
            if(db.Theatres.Find(theatreID) == null && userTextLower != "")
            {
                var movies = db.Cinemas.Include(c => c.Screenings.Select(m => m.Movie));
                foreach (var moviesaux in movies)
                {
                    foreach (var screeningsAux in moviesaux.Screenings)
                    {
                        if (screeningsAux.Movie.Name.ToLower().Contains(userTextLower))
                        {
                            moviesInSearch.Add(screeningsAux.Movie);
                        }


                        if (screeningsAux.Movie is Commercial)
                        {
                            var movie = (Commercial)screeningsAux.Movie;
                            if (movie.Actor.ToLower().Contains(userTextLower))
                            {
                                moviesInSearch.Add(screeningsAux.Movie);
                            }

                        }

                        if (screeningsAux.Movie is Documentary)
                        {
                            var movie = (Documentary)screeningsAux.Movie;
                            if (movie.Category.Equals(userTextLower))
                            {
                                moviesInSearch.Add(screeningsAux.Movie);
                            }
                        }

                    }
                }
                moviesInSearch.OrderBy(m => m.Name);
                return View("Index", moviesInSearch);
            }
            if (theatreID == null && userTextLower == "")
            {
                var movies = db.Movies.ToList();
                foreach (var item in movies)
                {
                    moviesInSearch.Add(item);
                }
                return View("Index", movies);
            }
            else
            {
                var movies = db.Cinemas
                .Include(c => c.Screenings.Select(m => m.Movie))
                .Where(t => t.TheatreID == theatreID);
                foreach (var moviesaux in movies)
                {
                    foreach (var screeningsAux in moviesaux.Screenings)
                    {
                        if (screeningsAux.Cinema.TheatreID == theatreID)
                        {
                            moviesInSearch.Add(screeningsAux.Movie);
                        }

                    }
                }
                moviesInSearch.OrderBy(m => m.Name);
                return View("Index", moviesInSearch);
            }


        }



        public ActionResult GetLogin()
        {
            return View("Login");

        }


        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            User user = new User();
            user.UserName = username;
            user.Password = password;
            Session["Admin"] = user;
            return RedirectToAction("IsAdmin",user);
        }


        
        public ActionResult IsAdmin(User logUser)
        {

            User user = logUser;
            User admin = db.Users.Where(a => a.UserName.Equals(user.UserName) && a.Password.Equals(user.Password)).FirstOrDefault();
            if (admin != null)
            {
                Session["Admin"] = admin;
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            Session["Admin"] = null;
            ViewBag.Error = "No coinciden las credenciales";
            return View("Error");
        }


        public ActionResult LogOut()
        {
            Session.Abandon();

            return RedirectToAction("Index");
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var documentary = db.Documentaries.Find(id);
            if (documentary == null)
            {
                return HttpNotFound();
            }

            ViewBag.Category = documentary.Category;
            var screenings = db.Screenings.Where(a => a.MovieID.Equals (documentary.MovieID)).ToList();
            var detailsNewModel = new DetailsViewModel {
                screenings = screenings.ToList(),
                movie = documentary};
            return View(detailsNewModel);
        }

        public ActionResult DetailsCom(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var commercial = db.Commercials.Find(id);
            if (commercial == null)
            {
                return HttpNotFound();
            }
            ViewBag.Actor = commercial.Actor;
            var screenings = db.Screenings.Where(a => a.MovieID.Equals(commercial.MovieID)).ToList();
            var detailsNewModel = new DetailsViewModel
            {
                screenings = screenings.ToList(),
                movie = commercial
            };
            return View("Details",detailsNewModel);
        }
    }
}