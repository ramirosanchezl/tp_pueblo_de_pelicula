﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pueblodepelicula.DAL;
using pueblodepelicula.Filters;
using pueblodepelicula.Models;

namespace pueblodepelicula.Controllers
{
    [UserFilter]
    public class MoviesController : Controller
    {
        private DBContext db = new DBContext();


        public ActionResult Index()
        {
            return View(db.Movies.ToList());
        }


        public ActionResult Type()
        {
            return View();
        }


        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var movie = db.Movies.Find(id);
            if (movie is Documentary)
            {
                return RedirectToAction("Details", "Documentaries", new { id });
            }
            else
            {
                return RedirectToAction("Details", "Commercials", new { id });

            }
        }

       

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var movie = db.Movies.Find(id);
            if (movie is Documentary)
            {
                return RedirectToAction("Edit", "Documentaries", new { id });
            }
            else
            {
                return RedirectToAction("Edit", "Commercials", new { id });

            }
        }


        

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Movie movie = db.Movies.Find(id);
            var screenings = db.Screenings.ToList();

            foreach (var item in screenings)
            {
                if (item.MovieID == id)
                {
                    ViewBag.Error = "No se puede eliminar una pelicula que esta en cartelera";
                    return View("Error");
                }
            }
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Movie movie = db.Movies.Find(id);
            db.Movies.Remove(movie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult LogOut()
        {
            Session.Abandon();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
