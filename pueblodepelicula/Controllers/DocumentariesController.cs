﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pueblodepelicula.DAL;
using pueblodepelicula.Filters;
using pueblodepelicula.Models;

namespace pueblodepelicula.Controllers
{
    [UserFilter]
    public class DocumentariesController : Controller
    {
        private DBContext db = new DBContext();


 


        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Documentary documentary, HttpPostedFileBase file)
        {
            var fileStream = file.InputStream;
            var binaryReader = new BinaryReader(fileStream);
            var bytes = binaryReader.ReadBytes((Int32)fileStream.Length);
            var base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
            documentary.Photo = base64String;
            if (documentary.RunTime > 360)
            {
                ViewBag.Error = "No se puede crear una pelicula de mas de 360 minutos";
                return View("Error");
            }
            db.Movies.Add(documentary);
            db.SaveChanges();
            return RedirectToAction("Index", "Movies", new { area = "" });

        }


       

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Documentary documentary = db.Documentaries.Find(id);
            if (documentary == null)
            {
                return HttpNotFound();
            }
            return View(documentary);
        }

       
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Documentary documentary = db.Documentaries.Find(id);
            db.Movies.Remove(documentary);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
