﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using pueblodepelicula.DAL;
using pueblodepelicula.Filters;
using pueblodepelicula.Models;

namespace pueblodepelicula.Controllers
{
    [UserFilter]
    public class ScreeningsController : Controller
    {
        private DBContext db = new DBContext();


        public ActionResult Index()
        {
            var screenings = db.Screenings.Include(s => s.Cinema).Include(s => s.Movie);
            return View(screenings.ToList());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Screening screening = db.Screenings.Find(id);
            if (screening == null)
            {
                return HttpNotFound();
            }
            return View(screening);
        }

        public ActionResult Create(int? theatre)
        {
            ViewBag.TheatreID = theatre;
            var cinemas = db.Cinemas.Where(a => a.TheatreID == theatre);
            ViewBag.CinemaID = cinemas.ToList();
            ViewBag.MovieID = new SelectList(db.Movies, "MovieID", "Name");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Screening screening)
        {
            var cinema = db.Cinemas.Find(screening.CinemaID);
            screening.Cinema = cinema;
            var theatre = db.Theatres.Find(screening.Cinema.TheatreID);
            screening.Cinema.Theatre = theatre;
            var dateAndTime = DateTime.Now;
            var date = dateAndTime.Date;
            screening.StartTime = date;
            if (db.Movies.Find(screening.MovieID) is Documentary)
            {
                if (db.Cinemas.Find(screening.CinemaID) is HD)
                {
                    ViewBag.Error = "No se puede crear una funcion de un documental en una sala HD";
                    return View("Error");
                }
            }

            var day = screening.StartTime;

            var auxScreenings = db.Screenings.ToList();
            auxScreenings = auxScreenings.Where(f => f.CinemaID == screening.CinemaID).ToList();

            var screenings = new List<Screening>();


            foreach (var a in auxScreenings)
            {
                if (a.StartTime.Date == day)
                {
                    screenings.Add(a);
                }

                if (a.StartTime.Date == day.AddDays(1) && a.StartTime.Hour < 10)
                {
                    screenings.Add(a);
                }
            }

            if (!screenings.Any() || screenings.Last().StartTime.Hour < 10 && screenings.Last().StartTime.Date == screening.StartTime.Date)
            {
                TimeSpan ts = new TimeSpan(10, 0, 0);
                screening.StartTime = screening.StartTime.Date + ts;
            }
            else
            {
                var lastScreening = screenings.Last();
                var startTime = lastScreening.StartTime.AddMinutes(db.Movies.Find(lastScreening.MovieID).RunTime);
                startTime = startTime.AddMinutes(15);

                TimeSpan ts = new TimeSpan(2, 0, 0);
                var maxStartTime = day + ts;
                maxStartTime = maxStartTime.AddDays(1);

                if (startTime < maxStartTime)
                {
                    screening.StartTime = startTime;
                }
                else
                {
                    ViewBag.Error = "Las funciones no pueden iniciar despues de las 02 AM del día siguiente";
                    return View("Error");
                }


            }

            if (ModelState.IsValid)
            {
                db.Screenings.Add(screening);
                db.SaveChanges();
                return RedirectToAction("Index");
            }



            ViewBag.CinemaID = new SelectList(db.Cinemas, "CinemaID", "CinemaID", screening.CinemaID);
            ViewBag.MovieID = new SelectList(db.Movies, "MovieID", "Name", screening.MovieID);
            return View(screening);

        }






        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Screening screening = db.Screenings.Find(id);
           


            if (screening == null)
            {
                return HttpNotFound();
            }
            return View(screening);
        }

        public ActionResult SelectTheatre()
        {
            ViewBag.Theatres = db.Theatres.ToList();
            return View();
        }
        public ActionResult SelectTheatreToDel()
        {
            ViewBag.Theatres = db.Theatres.ToList();
            return View();
        }

        public ActionResult SelectCinema(int? theatreID)
        {
            var cinemas = db.Cinemas.Where(a => a.TheatreID == theatreID);
            ViewBag.Cinemas = cinemas.ToList();
            return View();
        }

 
        public ActionResult VDeleteScreenings(int? cinemaID)
        {
            Session["CinemaID"] = cinemaID;
            var cinema = db.Cinemas.Find(cinemaID);
            var screeningsInCinema = db.Screenings.Where(a => a.CinemaID == cinema.CinemaID).Include(a => a.Movie);
            ViewBag.Screenings = screeningsInCinema.ToList();
            return View();
        }


        public ActionResult DeleteScreenings()
        {
            int cinemaToDel = (int)Session["CinemaID"];
            db.Screenings.RemoveRange(db.Screenings.Where(a => a.CinemaID == cinemaToDel));
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Screening screening = db.Screenings.Find(id);
            db.Screenings.Remove(screening);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult LogOut()
        {
            Session.Abandon();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
