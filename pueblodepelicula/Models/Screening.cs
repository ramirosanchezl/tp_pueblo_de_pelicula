﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pueblodepelicula.Models
{
    public class Screening
    {
        public int ScreeningID { get; set; }

        [ForeignKey("Movie")]
        public virtual int MovieID { get; set; }
        public virtual Movie Movie { get; set; }

        [ForeignKey("Cinema")]
        public virtual int CinemaID { get; set; }
        public virtual Cinema Cinema { get; set; }


        public DateTime StartTime { get; set; }

    }
}