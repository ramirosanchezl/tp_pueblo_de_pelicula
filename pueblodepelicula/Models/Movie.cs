﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pueblodepelicula.Models
{
    public abstract class Movie
    {
        public int MovieID { get; set; }

        [Required]
        [MaxLength(60)]
        public string Name { get; set; }
        [Required]
        public int RunTime { get; set; }

        [MaxLength(500)]
        public string Synopsis { get; set; }

        [Required]
        public string Photo { get; set; }

    }
}