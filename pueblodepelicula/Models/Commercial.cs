﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace pueblodepelicula.Models
{
    public class Commercial : Movie
    {
        [MaxLength(60)]
        public string Actor { get; set; }

    }
}