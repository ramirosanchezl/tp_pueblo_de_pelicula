﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pueblodepelicula.Models
{
    public class Theatre
    {
        public int TheatreID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public virtual ICollection<Cinema> Cinemas { get; set; }
    }
}