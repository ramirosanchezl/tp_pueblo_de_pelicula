﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pueblodepelicula.Models
{
    public class DetailsViewModel
    {
        public List<Screening> screenings = new List<Screening>();

        public Movie movie;
    }
}