﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace pueblodepelicula.Models
{
    public abstract class Cinema
    {
        public int CinemaID { get; set; }

        public int Number { get; set; }

        public int TheatreID { get; set; }
        public virtual Theatre Theatre { get; set; }

        public List<Screening> Screenings { get; set; }
    }
}