﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using pueblodepelicula.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace pueblodepelicula.DAL
{
    public class DBContext : DbContext
    {
        public DBContext() : base("DBContext")
        {
        }



        public DbSet<Cinema> Cinemas { get; set; }
        public DbSet<HD> HDs { get; set; }
        public DbSet<Normal> Normals { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Screening> Screenings { get; set; }
        public DbSet<Theatre> Theatres { get; set; }
        public DbSet<Commercial> Commercials { get; set; }
        public DbSet<Documentary> Documentaries { get; set; }
        public DbSet<User> Users { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
