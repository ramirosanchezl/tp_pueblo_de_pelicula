namespace pueblodepelicula.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cinema",
                c => new
                    {
                        CinemaID = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        TheatreID = c.Int(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.CinemaID)
                .ForeignKey("dbo.Theatre", t => t.TheatreID, cascadeDelete: true)
                .Index(t => t.TheatreID);
            
            CreateTable(
                "dbo.Screening",
                c => new
                    {
                        ScreeningID = c.Int(nullable: false, identity: true),
                        MovieID = c.Int(nullable: false),
                        CinemaID = c.Int(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ScreeningID)
                .ForeignKey("dbo.Cinema", t => t.CinemaID, cascadeDelete: true)
                .ForeignKey("dbo.Movie", t => t.MovieID, cascadeDelete: true)
                .Index(t => t.MovieID)
                .Index(t => t.CinemaID);
            
            CreateTable(
                "dbo.Movie",
                c => new
                    {
                        MovieID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 60),
                        RunTime = c.Int(nullable: false),
                        Synopsis = c.String(maxLength: 500),
                        Photo = c.String(nullable: false),
                        Actor = c.String(maxLength: 60),
                        Category = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.MovieID);
            
            CreateTable(
                "dbo.Theatre",
                c => new
                    {
                        TheatreID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.TheatreID);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserID = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false),
                        Password = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cinema", "TheatreID", "dbo.Theatre");
            DropForeignKey("dbo.Screening", "MovieID", "dbo.Movie");
            DropForeignKey("dbo.Screening", "CinemaID", "dbo.Cinema");
            DropIndex("dbo.Screening", new[] { "CinemaID" });
            DropIndex("dbo.Screening", new[] { "MovieID" });
            DropIndex("dbo.Cinema", new[] { "TheatreID" });
            DropTable("dbo.User");
            DropTable("dbo.Theatre");
            DropTable("dbo.Movie");
            DropTable("dbo.Screening");
            DropTable("dbo.Cinema");
        }
    }
}
